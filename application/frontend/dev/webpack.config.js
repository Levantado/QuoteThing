let webpack = require('webpack')
let path = require('path')
let pkg     = require('./package.json')
const MinifyPlugin = require("babel-minify-webpack-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackNotifierPlugin = require('webpack-notifier')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HappyPack = require('happypack');
HappyPack.SERIALIZABLE_OPTIONS.push('vue')
let happyThreadPool = HappyPack.ThreadPool({ size: 4 });

module.exports = {
    entry: {
        app: './src/index.js',
        vendor : Object.keys(pkg.dependencies)
    },
    //   devtool: 'inline-source-map',
    // devServer: {
    //   contentBase: './dist'
    // },
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: 'static/js/[name].js',
        chunkFilename: ('static/js/[id].js'),
    },
    plugins: [
        new HappyPack({
            id: 'eslint',
            verbose: false,
            threadPool: happyThreadPool,
            loaders: [ 'eslint-loader' ],
        }),
        new HappyPack({
            id: 'babel',
            verbose: false,
            threadPool: happyThreadPool,
            loaders: [ 'babel-loader?presets[]=env' ],
        }),
        new HappyPack({
            id: '1',
            verbose: false,
            threadPool: happyThreadPool,
            loaders: [{loader: 'vue-loader',
                options: {
                    extractCSS: true
                }}]
        }),
        new CleanWebpackPlugin(['../dist']),
        new ExtractTextPlugin({
            filename: 'static/css/[name].css'
        }),

        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/index.html',
            inject: true,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true

            },
            chunksSortMode: 'dependency'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: function (module) {
                // any required modules inside node_modules are extracted to vendor
                return (
                    module.resource &&
                    /\.js$/.test(module.resource) &&
                    module.resource.indexOf(
                        path.join(__dirname, '../node_modules')
                    ) === 0
                )
            }
        }),
        // extract webpack runtime and module manifest to its own file in order to
        // prevent vendor hash from being updated whenever app bundle is updated
        new webpack.optimize.CommonsChunkPlugin({
            name: 'manifest',
            chunks: ['vendor']
        }),
        new MinifyPlugin({},{
            comments: /^\**!|@preserve|@license|@cc_on/
        }),
        new WebpackNotifierPlugin(),
    ],
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.vue$/,
                include: path.resolve(__dirname, "src"),
                loaders:  'happypack/loader?id=eslint',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                include: path.resolve(__dirname, "src"),
                loader: "style-loader!css-loader"
            },
            {
                test: /\.vue$/,
                include: path.resolve(__dirname, "src"),
                loaders:  ['happypack/loader?id=1']},
            {
                test: /\.js$/,
                exclude: /node_modules/,
                include: path.resolve(__dirname, "src"),
                loaders: [ 'happypack/loader?id=babel' ],
            },
            {
                test: /\.(png|svg|jpe?g|gif)$/,
                include: path.resolve(__dirname, "src"),
                use: {
                    loader: 'file-loader',
                    options: {
                        loader: 'image-webpack-loader',
                        options: {
                            mozjpeg: {
                                progressive: true,
                                quality: 1
                            },
                            webp: {
                                quality: 75
                            }},
                        outputPath: "static/assets/",
                    }
                },


            }
        ]
    },

    resolve: {
        extensions: ['.js', '.vue', '.json', 'css', 'scss'],
        alias: {
            'vue$': 'vue/dist/vue.min.js',
            'axios$': 'axios/dist/axios.min.js',
        }
    }

};

if (process.env.NODE_ENV === 'production') {
    module.exports.plugins.push(
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: 'production',
                BABEL_ENV: 'production'
            }
        })
    )
}
