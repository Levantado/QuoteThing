let webpack = require('webpack')
let path = require('path')
let pkg     = require('./package.json')
const MinifyPlugin = require("babel-minify-webpack-plugin")
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackNotifierPlugin = require('webpack-notifier')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')

module.exports = {


  entry: {
    app: './src/index.js',
    vendor  : Object.keys(pkg.dependencies)
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'static/js/[name].js',
    chunkFilename: ('static/js/[id].js'),
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new ExtractTextPlugin({
      filename: 'static/css/[name].css'
    }),
    new OptimizeCSSPlugin({
      cssProcessorOptions: {
        safe: true
      }
    }),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/index.html',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true

      },
      chunksSortMode: 'dependency'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module) {
        // any required modules inside node_modules are extracted to vendor
        return (
          module.resource &&
          /\.js$/.test(module.resource) &&
          module.resource.indexOf(
            path.join(__dirname, '../node_modules')
          ) === 0
        )
      }
    }),
    // extract webpack runtime and module manifest to its own file in order to
    // prevent vendor hash from being updated whenever app bundle is updated
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
      chunks: ['vendor']
    }),
    new WebpackNotifierPlugin(),
    new MinifyPlugin({},{
      comments: /^\**!|@preserve|@license|@cc_on/
    })

  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        include: path.resolve(__dirname, "src"),
        loader: 'vue-loader',
        options: {
          extractCSS: true
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        include: path.resolve(__dirname, "src"),
        loader: 'babel-loader',
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/,
          include: path.resolve(__dirname, "src"),
         use: {
          loader: 'file-loader',
             options: {
                 loader: 'image-webpack-loader',
                 options: {
        mozjpeg: {
          progressive: true,
          quality: 1
        },
                 webp: {
          quality: 75
        }},
                 outputPath: "static/assets/",
        }
         },


      },
    ]
  },

  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.js',
      'axios$': 'axios/dist/axios.min.js',
      'vue-router$': 'vue-router/dist/vue-router.min.js',
    }
  }

};

if (process.env.NODE_ENV === 'production') {
  module.exports.plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: 'production',
        BABEL_ENV: 'production'
      }
    })
  )
}
