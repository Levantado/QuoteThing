const {
    FuseBox,
    VueComponentPlugin,
    QuantumPlugin,
    UglifyESPlugin,
    HTMLPlugin,
    SassPlugin,
    CSSPlugin,
    CSSResourcePlugin,
    WebIndexPlugin,
    Sparky
} = require("fuse-box");

let fuse;
let isProduction = true;

Sparky.task("set-prod", () => {
    isProduction = true;
});
Sparky.task("clean", () => Sparky.src("./dist").clean("dist/"));
// Sparky.task("watch-assets", () => Sparky.watch("./assets", { base: "./src" }).dest("./dist"));
// Sparky.task("copy-assets", () => Sparky.src("./assets", { base: "./src" }).dest("./dist"));

Sparky.task("config", () => {
    fuse = FuseBox.init({
        alias: {
            "vue$": "vue/dist/vue.min.js",
            "axios$": "axios/dist/axios.min.js",
        },
        target: 'browser',
        homeDir: "./src",
        output: "../dist/$name.js",
        cache: true,
        hash: isProduction,
        sourceMaps: !isProduction,
        useTypescriptCompiler: false,
        polyfillNonStandardDefaultUsage: true,
        plugins: [
             isProduction && UglifyESPlugin(),
            VueComponentPlugin({
                style: [
                    SassPlugin({
                        importer: true,
                    }),
                    CSSResourcePlugin(),
                    CSSPlugin({
                        group: 'components.css',
                        // outFile: './dist/component.css'
                        inject: 'components.css'
                    })
                ]
            }),
            SassPlugin(),
            CSSPlugin(),
            WebIndexPlugin({
                template: "./src/index.html",
            }),
            isProduction && QuantumPlugin({
                bakeApiIntoBundle: "vendor",
                uglify: false,
                treeshake: true
            }),
        ]
    });

    // if(!isProduction){
    //     fuse.dev({
    //         open: true,
    //         port: 8080
    //     });
    // }

    const vendor = fuse.bundle("vendor")
        .instructions("~ index.js");

    const app = fuse.bundle("app")
        .instructions("> [index.js]");

    if(!isProduction){
        app.watch().hmr();
    }
})

Sparky.task("default", ["clean", "watch-assets", "config"], () => {
    return fuse.run();
});

Sparky.task("dist", [ "clean", "copy-assets", "set-prod", "config"], () => {
    return fuse.run();
});