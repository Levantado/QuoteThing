module.exports = { contents: "\"use strict\";\nObject.defineProperty(exports, \"__esModule\", { value: true });\nvar vue_min_js_1 = require(\"vue/dist/vue.min.js\");\nvar App_vue_1 = require(\"./App.vue\");\nvue_min_js_1.default.config.productionTip = false;\nnew vue_min_js_1.default({\n    el: '#app',\n    template: '<App/>',\n    components: { App: App_vue_1.default }\n});\n",
dependencies: ["fuse-heresy-default","vue/dist/vue.min.js","./App.vue"],
sourceMap: {},
headerContent: undefined,
mtime: 1510220372204,
devLibsRequired : undefined,
_ : {}
}
