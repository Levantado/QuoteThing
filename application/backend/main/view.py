from . import bp_main
from flask import current_app
from backend.cel.task_spider import run_spider
from celery.result import AsyncResult, ResultBase

@bp_main.route('/')
def index():
    return current_app.send_static_file('index.html')

@bp_main.route('/run_spider/<text>')
def crawler(text):
    x = run_spider.apply_async([text])
    res = AsyncResult(x.id)
    ddd = 'Task id: {} , result: {}, state: {}, status: {}, some: {}'.format(res.task_id, res.result, res.state, res.status, res.get())
    return ddd

@bp_main.route('/check_spider/<id>')
def checker(id):
    res = AsyncResult(id)
    ddd = 'Task id: {} , result: {}, state: {}, status: {}, some: {}'.format(res.task_id, res.result, res.state,
                                                                             res.status, res.get())
    return ddd