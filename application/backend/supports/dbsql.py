import psycopg2

class Poster:

    def __init__(self):
        self.con = psycopg2.connect("dbname='quotesdb' user='admin' password='fghjrcbv9' host='db'")
        self.cur = self.con.cursor()



    def create_tables(self):
        create_author = '''CREATE TABLE IF NOT EXISTS authors (
                      id SERIAL PRIMARY KEY,
                      name VARCHAR UNIQUE)
        '''
        create_quotes = '''CREATE TABLE IF NOT EXISTS quotes (
                       id SERIAL PRIMARY KEY ,
                       text TEXT NOT NULL UNIQUE ,
                       author_id INTEGER,
                       FOREIGN KEY(author_id) REFERENCES authors(id)
         )'''
        create_tags = '''CREATE TABLE IF NOT EXISTS tags (
                       id SERIAL PRIMARY KEY,
                       tag VARCHAR NOT NULL UNIQUE
         )'''

        create_tagger = '''CREATE TABLE IF NOT EXISTS tag_quote (
                      tag_id INTEGER,
                      quote_id INTEGER,
                      FOREIGN KEY(tag_id) REFERENCES tags(id),
                      FOREIGN KEY(quote_id) REFERENCES quotes(id)
        )'''

        self.cur.execute(create_author)
        self.cur.execute(create_quotes)
        self.cur.execute(create_tags)
        self.cur.execute(create_tagger)
        self.con.commit()

    def insert_data(self, author,tags,quote):
        insert_author = '''INSERT INTO authors (name) VALUES (%s) ON CONFLICT (name) DO NOTHING '''
        insert_tags = '''INSERT INTO tags (name) VALUES (%s) ON CONFLICT (name) DO NOTHING'''
        insert_quote = '''INSERT INTO quotes (text, author_id) VALUES (
           %s,(SELECT id FROM authors WHERE name=%s)) ON CONFLICT (text) DO NOTHING RETURNING id'''
        chain_taggs = '''INSERT INTO tag_quote (tags_id, quotes_id) VALUES (
             (SELECT id FROM tags WHERE name=%s), %s)'''
        self.cur.execute(insert_author, (author,))
        self.con.commit()
        self.cur.execute(insert_quote, (quote, author,))
        self.con.commit()
        try:
            a = self.cur.fetchone()[0]
        except TypeError:
            a = None
        for tag in tags:
            if a is None:
                break
            self.cur.execute(insert_tags, (tag,))
            self.cur.execute(chain_taggs, (tag,a,))
            self.con.commit()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.con.close()