# -*- coding: utf-8 -*-
import scrapy
from scrapy.loader.processors import Compose
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
import re


class FirsttrySpider(CrawlSpider):
    name = 'firsttry'


    custom_settings = {
        'ITEM_PIPELINES': {
            'backend.supports.pipelines.QuotescrawlPipeline': 300,
        },
    }

    def __init__(self, *args, **kwargs):
        super(FirsttrySpider, self).__init__(*args, **kwargs)
        self.start_urls = [kwargs.get('start_url')]

    rules = [
        Rule(LinkExtractor(restrict_xpaths=('//a[@class="next_page"]',)),
                            follow=True, callback="parse_a")]
    def parse_a(self, response):
        i = {}
        node = response.xpath("/html/body/div[1]/div[2]/div[1]/div[2]/div[3]/div/div[1]")
        for x in node[:-1]:
            proc = Compose(lambda x:
                           x.replace('\n','').replace('“','').replace('”','').lstrip().rstrip())
            d = x.xpath("./div[@class='quoteText']/text()").extract()
            d = ''.join(d)
            d = proc(d)
            d = re.sub('(\W{2,}―\W{0,})','',d)
            d = re.sub('(―\W{0,})','',d)+'.'
            if  d == '.':
               pass
            else:
                try:
                    i['author'] = x.xpath("./div[@class='quoteText']/a[@class='authorOrTitle']/text()").extract()[0]
                    i['url'] = x.xpath("./div[@class='quoteText']/a[@class='authorOrTitle']/@href").extract()[0]
                except IndexError:
                    print(d)
                    pass
                else:
                    i['text'] = d
                    i['tags'] = x.xpath("./div[@class='quoteFooter']/div[1]/a/text()").extract()
            yield i
    parse_start_url = parse_a
