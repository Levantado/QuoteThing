# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

# from .SORM import Quote, Author, Tag, db
# from backend.db import db
# from backend.model import Quote,Author,Tag
from backend.model import Quote, Author, Tag
from backend.model import db


class QuotescrawlPipeline(object):
    def process_item(self, item, spider):
        def get_or_create(session, model, flag=False, **kwargs):
            instance = session.query(model).filter_by(**kwargs).first()
            if instance:
                return instance
            elif flag:
                instance = model(**kwargs)
                return instance
            else:
                instance = model(**kwargs)
                session.add(instance)
                session.commit()
                return instance
        author = get_or_create(db.session, Author, name=item['author'], url=item['url'])
        quote = get_or_create(db.session, Quote, text=item['text'], author=author)
        tager = []
        for tag in item['tags']:
            tag = get_or_create(db.session, Tag, name=tag)
            tager.append(tag)
        quote.tags = tager
        db.session.add(quote)
        db.session.commit()

        return item
