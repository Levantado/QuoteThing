from urllib.request import urlopen
from lxml import html
from .dbsql import Poster
import logging

FORMAT = '%(asctime)s %(levelname)-8s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

url = 'https://www.goodreads.com/quotes/tag/'


def parser(qw,c):
    logging.debug('Parsing elements of data')
    for quote in qw:
        c += 1
        quote_wr = quote.xpath("./div[@class='quoteText']")[0]
        quote_text = ''.join(quote_wr.xpath("./text()")).replace('\n','').replace("  ",'').replace("―,",'').replace('\u201c','').replace('\u201d','')
        quote_author = quote_wr.xpath("./*[@class='authorOrTitle']/text()")[0]
        quote_tags = quote.xpath("./div[@class='quoteFooter']/div[contains(@class,'greyText')]/a/text()")
        logging.debug('Text:' + quote_text)
        logging.debug('Author:' +quote_author)
        logging.debug('Tags:' + ' '.join(quote_tags))
        logging.debug('Parse element end')
        logging.debug('Start writing in DB')
        db = Poster()
        db.insert_data(quote_author,quote_tags,quote_text)
        logging.debug('Writing in DB end')
        del(db)
    logging.debug('Parsing page and write to DB end')
counter = 0

def main(tag):
    url = 'https://www.goodreads.com/quotes/tag/' + tag
    while True:
        logging.info('Connect to: %s',url)
        resp = urlopen(url).read()
        logging.debug('Connect succefuly')
        logging.debug('Take main data for parsing')
        raw_page = html.fromstring(resp)
        quote_wrap = raw_page.xpath("//div[contains(@class,'quoteDetails')]")
        logging.debug('Start parsing')
        parser(quote_wrap, counter)
        try:
            next_page = raw_page.xpath(
                '//a[@class="next_page"]/@href')[0]
        except IndexError:
            break
        url = 'https://www.goodreads.com' + next_page

