from . import api
from .route import PopTags, Tags, Authors, Tages

api.add_resource(PopTags, '/pop-tag')
api.add_resource(Tags, '/<tag>/<page>')
api.add_resource(Authors, '/author/<author_name>/<page>')
api.add_resource(Tages, '/tags/<path:fullurl>/<page>')
