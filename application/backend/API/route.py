# from backend.model import  Tag, Quote, association_table
# from backend.db import db
from backend.model import db as db2
from backend.model import Tag, Quote, association_table
# from ..supports.QuotesCrawl.SORM import Tag, Quote, association_table, db
from sqlalchemy import func, desc
from flask_restful import Resource

LIMIT_SHOW = 30


def quote_responser(data):
    quote = []
    if not data:
        return {'message': 'no data'}
    for y in data:
        tgs = [str(x) for x in y.tags]
        quote.append({"author": y.author.name, "text": y.text, "tags": tgs})
    return quote


class PopTags(Resource):
    def get(self):
        pop_tags_list = [{"name": str(x[0]), "count": x[1]} for x in
                         db2.session.query(Tag, func.count(association_table.c.tags_id).label('total'))
                             .join(association_table)
                             .group_by(Tag)
                             .order_by(desc('total'))
                             .limit(30)
                             .all()]
        return pop_tags_list


class Tags(Resource):
    def get(self, tag, page=1):
        pa = int(page)
        a = Quote.query.filter(Quote.tags.any(Tag.name == tag)).limit(LIMIT_SHOW).offset(LIMIT_SHOW * pa).all()
        # a = db.query(Quote).filter(Quote.tags.any(Tag.name == tag)).limit(LIMIT_SHOW).offset(LIMIT_SHOW * pa).all()
        return quote_responser(a)


class Authors(Resource):
    def get(self, author_name, page=1):
        pa = int(page)
        # a = db.query(Quote).filter(Quote.author.has(name=author_name)).limit(LIMIT_SHOW).offset(LIMIT_SHOW * pa).all()
        a = Quote.query.filter(Quote.author.has(name=author_name)).limit(LIMIT_SHOW).offset(LIMIT_SHOW * pa).all()
        return quote_responser(a)


class Tages(Resource):
    def get(self, page, fullurl=''):
        tags_list = fullurl.split(',')
        pa = int(page)
        a = Quote.query.join(Quote.tags).filter(Tag.name.in_(tags_list)).group_by(Quote.id).having(
            func.count(Quote.id) == len(tags_list)).limit(LIMIT_SHOW).offset(LIMIT_SHOW * pa).all()
        # a = db.query(Quote).join(Quote.tags).filter(Tag.name.in_(tags_list)).group_by(Quote.id).having(
        #     func.count(Quote.id) == len(tags_list)).limit(LIMIT_SHOW).offset(LIMIT_SHOW * pa).all()
        return quote_responser(a)
