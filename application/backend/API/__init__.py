from flask import Blueprint
from flask_restful import Api

bp_api = Blueprint('APIS', __name__)
api = Api(bp_api)

from . import route
from . import resource