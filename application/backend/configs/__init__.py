class BaseConfig:
    """Base configuration"""
    DEBUG = False
    TESTING = False


class DevelopmentConfig(BaseConfig):
    """Development configuration"""
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://admin:fghjrcbv9@db:5432/quotesdb'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    BROKER_URL = 'pyamqp://guest@localhost//'

class TestingConfig(BaseConfig):
    """Testing configuration"""
    DEBUG = True
    TESTING = True


class ProductionConfig(BaseConfig):
    """Production configuration"""
    DEBUG = False
    DB = 'postgresql+psycopg2://admin:fghjrcbv9@db:5432/quotesdb'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    BROKER_URL = 'pyamqp://guest@db//'
