# from scrapy.crawler import CrawlerRunner, CrawlerProcess
# from backend.supports.firsttry import FirsttrySpider
from celery import Celery
# from celery.signals import worker_process_init
# from backend import create_app
from backend.supports.scraper import main
celery  = Celery('tasks', broker='amqp://guest@rabbitmq//', backend='amqp://guest@rabbitmq//')

# @worker_process_init.connect
# def init_celery_flask_app(**kwargs):
#     app = create_app()
#     app.app_context().push()

@celery.task
def run_spider(text):
    main(text)
    # runner = CrawlerProcess()
    # runner.crawl(FirsttrySpider, start_url='https://www.goodreads.com/quotes/tag/' + text)
    # runner.start()
    # runner.stop()
    return
