from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

association_table = db.Table(
    'tag_quote',
    db.Model.metadata,
    db.Column('quotes_id', db.Integer, db.ForeignKey('quotes.id')),
    db.Column('tags_id', db.Integer, db.ForeignKey('tags.id'))
)


class Author(db.Model):
    __tablename__ = 'authors'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, unique=True)
    url = db.Column(db.Text)
    quotes = db.relationship("Quote")

    def __init__(self, name, url):
        self.name = name
        self.url = url

    def __repr__(self):
        return f"Author: {self.name} {self.url}"


class Tag(db.Model):
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, unique=True)
    count = db.Column(db.Integer)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f"{self.name}"


class Quote(db.Model):
    __tablename__ = 'quotes'

    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey('authors.id'))
    text = db.Column(db.Text, unique=True)
    author = db.relationship("Author", back_populates="quotes")
    tags = db.relationship("Tag", secondary=association_table)

    def __repr__(self):
        return f"Quote: {self.author} {self.text}"


# db.Model.metadata.create_all(db.engine)
