from flask import Flask

def create_app():
    app = Flask(__name__, static_url_path='', static_folder='../frontend/dist')
    app.config.from_object('backend.configs.DevelopmentConfig')
    from .model import db
    db.init_app(app)
    from .API import bp_api
    app.register_blueprint(bp_api, url_prefix='/api')
    from .main import bp_main
    app.register_blueprint(bp_main)

    return app

