from backend import create_app
from flask import got_request_exception
import os
# import rollbar
# import rollbar.contrib.flask

app = create_app()
from backend.model import db

@app.before_first_request
def create_database():
    db.create_all()

# @app.before_first_request
# def init_rollbar():
#     rollbar.init(
#         '55f224e12ed0452a991ae888b3ea6443',
#         'flasktest',
#         root=os.path.dirname(os.path.realpath(__file__)),
#         allow_logging_basic_config=False)
#     got_request_exception.connect(rollbar.contrib.flask.report_exception, app)

# if __name__ == "__main__":
#     app.run()